from App.Models.Room import Room
from App.Models.RoomDefinition import RoomDefinition
from App.Models.Maze import Maze

class TestMaze():
    def test_createRoom(self):

        #arrange
        id = 1
        label = ""
        wallsWithDoors = ["E","S"]
        maze = Maze(id, label)
        
        #act
        room = maze.createRoom(id, label, wallsWithDoors)

        #assert
        assert(len(room.walls)== 4)
        for w in room.walls:
            if w.label == "E":
                assert(w.door is not None)
            elif w.label == "S":
                assert(w.door is not None)
            else:
                assert(w.door is None)
    
    def test_createMultipleRooms(self):
        # arrange
        maze = Maze(1, "smallMaze")
        rooms  = []
        
        # act
        room1 = maze.createRoom(0, "", ["N"])
        rooms.append(room1)

        room2 = maze.createRoom(2, "", ["S"])
        rooms.append(room2)

        # assert
        assert(len(rooms) == 2)

    def test_createRooms(self):

        #arrange
        maze = Maze(1, "jungle")
        roomdefinitions = []
        roomdefinitions.append(RoomDefinition(0, "start", ["N"]))
        roomdefinitions.append(RoomDefinition(1, "", ["S","E"]))
        roomdefinitions.append(RoomDefinition(2, "end", ["W"]))



        #act
        rooms = maze.createRooms(roomdefinitions)

        #assert

        assert(len(rooms)== 3)
        for room in rooms:
            if room.label == "start":
                for wall in room.walls:
                    if wall.label == "N":
                        assert wall.door is not None
                    else:
                        assert wall.door is None
            elif room.label == "end":
                for wall in room.walls:
                    if wall.label == "W":
                        assert wall.door is not None
                    else:
                        assert wall.door is None
            else:
                for wall in room.walls:
                    if wall.label == "S" or wall.label == "E":
                        assert wall.door is not None
                    else:
                        assert wall.door is None
