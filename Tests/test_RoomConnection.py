from App.Models.RoomConnections import RoomConnections
from App.Models.Room import Room
from App.Models.Wall import Wall
from App.Models.Door import Door
from App.Models.Maze import Maze

class TestRoomConnections():
    def test_roomconnections(self):
    
        #arrange
        id = 0
        label = "jungle"
        maze = Maze(id, label)
        roomStart = maze.createRoom(0, "start", ["N"])
        room2 = maze.createRoom(1, "", ["N","S"])
        rc = RoomConnections()

        #act
        rc.addConnection(roomStart, "N", room2)

        #assert
        room = rc.getRoomFor(roomStart, "N")
        assert(room.id == room2.id)
