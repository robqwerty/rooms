from App.Models.Maze import Maze

class TestRoom():
    def test_createStartRoom(self):
        # arrange
        label = "start"
        id = 0
        wallsWithDoors = ["N"]
        maze = Maze(id, label)
        
        # act
        room = maze.createRoom(id, label, wallsWithDoors)      
        # assert
        assert(room is not None)
        assert(len(room.walls)== 4)
        for wall in room.walls:
            if wall.label == "N":                    
                assert(wall.door is not None)
            else:
                assert(wall.door is None)

    def test_createEndRoom(self):
        # arrange
        label = "end"
        id = 3
        wallsWithDoors =["W"]
        maze = Maze(id, label)
        # act
        room = maze.createRoom(id, label, wallsWithDoors)        # assert
        assert(room is not None)
        assert(len(room.walls)== 4)
        for w in room.walls:
            if w.label == "W":
                assert(w.door is not None)
            else:
                assert(w.door is None)

    def test_createRoom1(self):
        # arrange
        label = ""
        id = 1
        wallsWithDoors =["N", "S"]
        maze = Maze(id, label)
        # act
        room = maze.createRoom(id, label, wallsWithDoors)        # assert
        assert(room is not None)
        assert(len(room.walls)== 4)
        for w in room.walls:
            if w.label == "N" or w.label == "S":
                assert(w.door is not None)
            else:
                assert(w.door is None)

    def test_createRoom2(self):
        # arrange
        label = ""
        id = 2
        wallsWithDoors =["E", "S"]
        maze = Maze(id, label)
         
        # act
        room = maze.createRoom(id, label, wallsWithDoors)
        # assert
        assert(room is not None)
        assert(len(room.walls)== 4)
        for w in room.walls:
            if w.label == "E" or w.label == "S":
                assert(w.door is not None)
            else:
                assert(w.door is None)
