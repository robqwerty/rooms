from App.Models.Wall import Wall #nodig voor test? ook als al geimporteerd is in test_RoomConnection
from App.Models.Room import Room
from App.Models.Door import Door


class Maze():
    def __init__(self, id, label):
        self.id = id
        self.label = label
        self.wallLabels = ["N","E","S","W"]

        
    def createRoom(self, id, label, wallsWithDoors):    #self ertussen uit gehaald
        walls =[]
        for l in self.wallLabels:
            if l in wallsWithDoors:
                wall = self._createWallWithDoor(l)
                walls.append(wall)
            else:
                wall = self._createWall(l)
                walls.append(wall)
        room = Room(id, label, walls)

        
        return room

    def _createWall(self, label):
        wall = Wall(label)
        return wall

    def _createWallWithDoor(self, label):
        door = Door()
        wall = Wall(label, door)

        return wall


    def createRooms(self, roomdefinitions):  
        rooms = []
        for x in roomdefinitions: # [0, "start",["N"]]
    
            room = self.createRoom(x.id, x.label, x.wallsWithDoors)
            rooms.append(room)
        return rooms

        