class RoomConnections():
    def __init__(self):
        self.connections = {}
        

    def addConnection(self, room, wall, nextRoom):
        key = self._createKey(room.id, wall)
        self.connections[key] = nextRoom


    def getRoomFor(self, room, wall):
        key = self._createKey(room.id, wall)
        return self.connections.get(key)

    def _createKey(self, id, wall):
        key = str(id) + wall